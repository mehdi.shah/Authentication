from django.contrib import auth
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode

from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.tokens import RefreshToken, TokenError

from authentication.models import User

# NEW
# from django.contrib.auth.models import User
# from django.contrib.auth import get_user_model
# User = get_user_model()

class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=68, min_length=6, write_only=True)

    default_error_messages = {
        'username': 'The username should only contain alphanumeric characters'}

    class Meta:
        model = User
        fields = ['first_name','last_name','email', 'username','password','phone']

    def validate(self, attrs):
        email = attrs.get('email', '')
        username = attrs.get('username', '')

        if not username.isalnum():
            raise serializers.ValidationError(
                self.default_error_messages)
        return attrs

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)





class EmailVerifySerializer(serializers.ModelSerializer):
    token = serializers.CharField(max_length=555)

    class Meta:
        model = User
        fields = ['token']



class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255, min_length=6)
    password = serializers.CharField(max_length=68, min_length=6)
    first_name = serializers.CharField(max_length=255, min_length=3, read_only=True)
    last_name = serializers.CharField(max_length=255, min_length=3, read_only=True)
    username = serializers.CharField(max_length=255, min_length=3, read_only=True)
    tokens = serializers.SerializerMethodField()


    def get_tokens(self, obj):
        user = User.objects.get(email=obj['email'])

        return {
            'access': user.tokens()['access'],
            'refresh': user.tokens()['refresh'],
        }
    

    class Meta:
        model = User
        fields = ('email', 'password', 'first_name', 'last_name', 'username', 'tokens')



    def validate(self, attrs):
        email = attrs.get('email', '')
        password = attrs.get('password', '')


        user = auth.authenticate(email=email, password=password)

        if not user:
            raise AuthenticationFailed("Invalid credentials.")
        if not user.is_active:
            raise AuthenticationFailed("Account disabled, please contact the admin.")
        if not user.email_verified:
            raise AuthenticationFailed("Email is not verified, please verify your email.")

        return {
            "email": user.email,
            "password": user.password,
            "first_name": user.first_name,
            "last_name":user.last_name,
            "username":user.username,
            "tokens": user.tokens(),
        }

        return super().validate(attrs)




class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    default_error_messages = {
        'bad_token': ('Token is expired or invalid')
    }

    def validate(self, attrs):
        self.token = attrs['refresh']
        return attrs

    def save(self, **kwargs):

        try:
            RefreshToken(self.token).blacklist()

        except TokenError:
            self.fail('bad_token')




class RequestPasswordResetEmailSerializer(serializers.Serializer):
    email = serializers.EmailField(min_length=2)

# برای عدم استفاده از مدل = یوزر توی پرانتز خط 89 از سرییالایزرزدات سرییالایزر استفاده میکنیم

    class Meta:
        fields = ['email',]




class SetNewPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(min_length=6, max_length=68, write_only=True)
    token = serializers.CharField(min_length=1, write_only=True)
    uidb64 = serializers.CharField(min_length=1, write_only=True)

    class Meta:
        fields = ['password', 'token', 'uidb64']

    def validate(self, attrs):
        try:
            password = attrs.get('password')
            token = attrs.get('token')
            uidb64 = attrs.get('uidb64')

            username = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(username=username)

            if not PasswordResetTokenGenerator().check_token(user,token):
                raise AuthenticationFailed("The reset link is invalid", 401)
            user.set_password(password)
            user.save()
            return (user)
        except Exception as e:
            raise AuthenticationFailed("The reset link is invalid", 401)
        return super().validate(attrs)



    

# NEW
class ChangePasswordSerializer(serializers.Serializer):
    class Meta:    
        model = User
        

    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True, min_length=6, max_length=68)
    new_password = serializers.CharField(required=True, min_length=6, max_length=68)



