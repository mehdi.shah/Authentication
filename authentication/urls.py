from django.urls import path

from authentication.views import RegisterAPIView, VerifyEmailAPIView, LoginAPIView, PasswordTokenCheckAPIView, RequestPasswordResetEmailAPIView, SetNewPasswordAPIView, LogoutAPIView , getPhoneNumberRegistered, getPhoneNumberRegistered_TimeBased, ChangePasswordView

from rest_framework_simplejwt.views import (
    TokenRefreshView,
)


urlpatterns = [
    path('register', RegisterAPIView.as_view(), name='register'),
    path('login', LoginAPIView.as_view(), name='login'),
    path('logout', LogoutAPIView.as_view(), name='logout'),
    path('verify-email', VerifyEmailAPIView.as_view(), name='verify-email'),
    path('password-reset/<uidb64>/<token>/',PasswordTokenCheckAPIView.as_view(), name='password-reset-confirm'),
    path('request-reset-password-email/', RequestPasswordResetEmailAPIView.as_view(), name='request-reset-password-email'),
    path('password-reset-complete', SetNewPasswordAPIView.as_view(),name='password-reset-complete'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('change-password/', ChangePasswordView.as_view(), name='change-password'),
    path("<phone>/", getPhoneNumberRegistered.as_view(), name="OTP Gen"),
    path("time_based/<phone>/", getPhoneNumberRegistered_TimeBased.as_view(), name="OTP Gen Time Based"),
]

