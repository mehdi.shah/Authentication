from django.shortcuts import render
from django.urls import reverse
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode


from rest_framework import generics, status, views, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.tokens import RefreshToken

from authentication.serializers import RegisterSerializer, EmailVerifySerializer, LoginSerializer, RequestPasswordResetEmailSerializer, SetNewPasswordSerializer, LogoutSerializer, ChangePasswordSerializer
from authentication.models import User, phoneModel
from authentication.utils import Util
from authentication.renderers import UserRenderer

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from datetime import datetime

import jwt
import pyotp
import base64

# این کد پایین برای وقتی است که کاستوم یوزرت رو میخوای تحمیل کنی
from django.contrib.auth import get_user_model
User = get_user_model()

#from twilio.rest import Client
from kavenegar import *



class RegisterAPIView(generics.GenericAPIView):

    serializer_class = RegisterSerializer
    renderer_classes = (UserRenderer,)


    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data=user, context={"request": request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user_data = serializer.data

        user = User.objects.get(email=user_data['email'])
        token = RefreshToken.for_user(user).access_token

        current_site = get_current_site(request).domain
        relativeLink = reverse("verify-email")
        absurl = "http://"+current_site+relativeLink+"?token=" +str(token)
        email_body = "Hi " +user.username + " Use link bellow to verify your email \n " + absurl
        data = {"email_body" : email_body, "to_email": user.email, "email_subject": "verify your email address"}
        Util.send_email(data)

        return Response(user_data, status=status.HTTP_201_CREATED)


class VerifyEmailAPIView(views.APIView):
    
    serializer_class = EmailVerifySerializer

    token_param_config = openapi.Parameter("token", in_=openapi.IN_QUERY, description="Enter your token", type=openapi.TYPE_STRING)

    @swagger_auto_schema(manual_parameters=[token_param_config])
    def get(self, request):
        token = request.GET.get("token")
# حتما اون اگوریتم گهو بنویس
        try:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
            user = User.objects.get(id = payload["user_id"])
            if not user.email_verified:
                user.email_verified = True
                user.save()
            return Response({"email":"Successfully activated"}, status=status.HTTP_200_OK)
        except jwt.ExpiredSignatureError as identifier:
            return Response({'error': 'Activation Expired'}, status=status.HTTP_400_BAD_REQUEST)
        except jwt.exceptions.DecodeError as identifier:
            return Response({'error': 'Invalid token'}, status=status.HTTP_400_BAD_REQUEST)




class LoginAPIView(generics.GenericAPIView):

    serializer_class = LoginSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)



class LogoutAPIView(generics.GenericAPIView):

    serializer_class = LogoutSerializer

    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)






class RequestPasswordResetEmailAPIView(generics.GenericAPIView):
    serializer_class = RequestPasswordResetEmailSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        
        email = request.data.get('email', '')

        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            uidb64 = urlsafe_base64_encode(smart_bytes(user.username))
            token = PasswordResetTokenGenerator().make_token(user)
            current_site = get_current_site(request=request).domain
            relativeLink = reverse("password-reset-confirm", kwargs={"uidb64": uidb64, "token": token})
            absurl = "http://"+current_site+relativeLink
            email_body = "Hello \n Use link bellow to reset your password. \n " + absurl
            data = {"email_body" : email_body, "to_email": user.email, "email_subject": "Reset your password"}
            Util.send_email(data)
        return Response({"success":"we have sent you a link to reset your password."}, status=status.HTTP_200_OK)




class PasswordTokenCheckAPIView(generics.GenericAPIView):
    
    def get(self, request, uidb64, token):

        try:
            username = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(username=username)

            if not PasswordResetTokenGenerator().check_token(user, token):
                return Response({"error":"Token is not valid, please request a new one."}, status=status.HTTP_401_UNAUTHORIZED)
            return Response({"success":True, "message":"Credentials valid", "uidb64": uidb64, "token":token},  status=status.HTTP_200_OK)

        except DjangoUnicodeDecodeError as identifier:
            if not PasswordResetTokenGenerator().check_token(user, token):
                return Response({"error":"Token is not valid, please request a new one."}, status=status.HTTP_401_UNAUTHORIZED)

            



class SetNewPasswordAPIView(generics.GenericAPIView):
    serializer_class = SetNewPasswordSerializer

    def patch(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'success': True, 'message': 'Password reset successfully'}, status=status.HTTP_200_OK)





class ChangePasswordView(generics.UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# This class returns the string needed to generate the key
class generateKey:
    @staticmethod
    def returnValue(phone):
        return str(phone) + str(datetime.date(datetime.now())) + "Some Random Secret Key"


class getPhoneNumberRegistered(APIView):
    # Get to Create a call for OTP
    @staticmethod
    def get(request, phone):
        try:
            Mobile = phoneModel.objects.get(Mobile=phone)  # if Mobile already exists the take this else create New One
        except ObjectDoesNotExist:
            phoneModel.objects.create(
                Mobile=phone,
            )
            Mobile = phoneModel.objects.get(Mobile=phone)  # user Newly created Model
        Mobile.counter += 1  # Update Counter At every Call
        Mobile.save()  # Save the data
        keygen = generateKey()
        key = base64.b32encode(keygen.returnValue(phone).encode())  # Key is generated
        OTP = pyotp.HOTP(key)  # HOTP Model for OTP is created
        print(OTP.at(Mobile.counter))
        # Using Multi-Threading send the OTP Using Messaging Services like Twilio or Fast2sms
        return Response({"otp": OTP.at(Mobile.counter)}, status=200)  # Just for demonstration

    # This Method verifies the OTP
    @staticmethod
    def post(request, phone):
        try:
            Mobile = phoneModel.objects.get(Mobile=phone)
        except ObjectDoesNotExist:
            return Response("User does not exist", status=404)  # False Call

        keygen = generateKey()
        key = base64.b32encode(keygen.returnValue(phone).encode())  # Generating Key
        OTP = pyotp.HOTP(key)  # HOTP Model
        if OTP.verify(request.data["otp"], Mobile.counter):  # Verifying the OTP
            Mobile.isVerified = True
            Mobile.save()
            
            return Response("You are authorized, thanks!", status=200)
        return Response("OTP is wrong, try again please.", status=400)





# Time after which OTP will expire
EXPIRY_TIME = 600 # seconds

class getPhoneNumberRegistered_TimeBased(APIView):
    # Get to Create a call for OTP
    @staticmethod
    def get(request, phone):
        try:
            Mobile = phoneModel.objects.get(Mobile=phone)  # if Mobile already exists the take this else create New One
        except ObjectDoesNotExist:
            phoneModel.objects.create(
                Mobile=phone,
            )
            Mobile = phoneModel.objects.get(Mobile=phone)  # user Newly created Model
        Mobile.save()  # Save the data
        keygen = generateKey()
        key = base64.b32encode(keygen.returnValue(phone).encode())  # Key is generated
        OTP = pyotp.TOTP(key,interval = EXPIRY_TIME)  # TOTP Model for OTP is created
        print(OTP.now())
        """
        account_sid = 'AC83bc1a654e85a044e85450f0ffe0a902'
        auth_token = 'e9e65a28e8a37a5df7fe926b28ff06d2'
        client = Client(account_sid, auth_token)
        message = client.messages.create(
            body='Hi there,',
            from_='+16179345585',
            to='+989197672179',
        )
        print(message.sid)"""

        """api = KavenegarAPI('414946694A6D49413554424D4356584D3359497461426235444F544C5343676C596138737A3042776437343D')
        params = { 'sender' : '1000596446', 'receptor': phone, 'message' : {"your activate code is: \n", OTP.now()}}    
        response = api.sms_send( params)"""
        # Using Multi-Threading send the OTP Using Messaging Services like Twilio or Fast2sms
        return Response({"otp": OTP.now()}, status=200)  # Just for demonstration

    # This Method verifies the OTP
    @staticmethod
    def post(request, phone):
        try:
            Mobile = phoneModel.objects.get(Mobile=phone)
        except ObjectDoesNotExist:
            return Response("User does not exist", status=404)  # False Call

        keygen = generateKey()
        key = base64.b32encode(keygen.returnValue(phone).encode())  # Generating Key
        OTP = pyotp.TOTP(key,interval = EXPIRY_TIME)  # TOTP Model 
        if OTP.verify(request.data["otp"]):  # Verifying the OTP
            Mobile.isVerified = True
            Mobile.save()
            return Response("You are authorized, thanks!", status=200)
        return Response("OTP is wrong/expired, try again please.", status=400)

