from django.shortcuts import render

from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import permissions, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from expenses.serializers import ExpensesSerializer
from expenses.models import Expense
from expenses.permissions import IsOwner
from expenses.pagination import CustomPageNumberPagination


class ExpenseListAPIView(ListCreateAPIView):
    serializer_class = ExpensesSerializer
    queryset = Expense.objects.all().order_by('-created_at')
    permission_classes = (permissions.IsAuthenticated,)

    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]

    filterset_fields = ["id","amount","category","description","date"]
    search_fields = ["id","amount","category","description","date"]
    ordering_fields = ["id","amount","category","description","date"] 
    pagination_class = CustomPageNumberPagination 

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user, is_deleted=False).order_by('-created_at')



class ExpenseDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ExpensesSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    queryset = Expense.objects.all().order_by('-created_at')
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user, is_deleted=False).order_by('-created_at')


