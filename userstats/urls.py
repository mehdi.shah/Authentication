from django.urls import path

from userstats.views import ExpensesSummaryStatsAPIView



urlpatterns = [
    path('', ExpensesSummaryStatsAPIView.as_view(), name='expense_category_data')
]