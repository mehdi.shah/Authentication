from django.shortcuts import render

from rest_framework import response, status
from rest_framework.views import APIView

from expenses.models import Expense

import datetime

class ExpensesSummaryStatsAPIView(APIView):

    def get_amount_for_category(self, expense_list, category):
        expenses = expense_list.filter(category=category)
        amount = 0

        for expense in expenses:
            amount += expense.amount

        return {"amount":str(amount)}

    def get_category(self, expense):
        return expense.category
    
    def get(self, request):
        todays_date = datetime.date.today()
        year_ago = todays_date-datetime.timedelta(days=30*12)
        expenses = Expense.objects.filter(owner=request.user, date__gte=year_ago, date__lte = todays_date)
        final = {}
        categories = list(set(map(self.get_category, expenses)))
        for expense in expenses:
            for category in categories:
                final[category] = self.get_amount_for_category(expenses, category)
        return response.Response({"category_data":final}, status=status.HTTP_200_OK)



         




